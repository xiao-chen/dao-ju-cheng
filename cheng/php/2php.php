<?php
       $con=new mysqli("localhost","szu","123456","propcity");

    // 编码格式
    $con->query("SET NAMES 'UTF8'");

    // 判断数据库连接是否成功
    if($con->connect_error){
        die("连接失败".$con->connect_error);
    }
    // 判断传输方式
    if($_GET){
        $type=$_GET["type"];

    }else{
        $type=$_POST["type"];
    }

    // 根据不同的值，进行不同的操作
    switch($type){
        // 主页商品
        case "shop":
            shop('zhuye');
        break;
        // 周边
        case 'rimm':
            shop('rim');
        break;
        // 热门商品的点击事件
        case "hotShop":
            shop('hot');
        break;
        // 主页的英雄
        case "zhero":
            shop("hero");
        break;
        // 删除主页元素
        case "rezhuye":
            del('zhuye');
        break;
        // 删除周边
        case 'rerim':
            del('rim');
        break;
        // 删除热门
        case "rehot":
            del('hot');
        break;
        // 删除英雄
        case "rehero":
            del("hero");
        break;
        // 删除皮肤
        case "reskin":
            del('skin');
        break;
        // 修改主页商品
        case "alter":
            alter("zhuye");
        break;
        // 修改热门商品
        case "hots":
            alter('hot');
        break;
        // 修改周边商城
        case "rimshop":
            alter('rim');
        break;
        // 皮肤商品
        case "skin":
            shop("skin");
        break;
        // 修改皮肤商品
        case "skinx":
            alter("skin");
        break;
        // 修改主页英雄
        case "hero":
            alter('hero');
        break;
        // 注册
        case "sign":
            sign();
        break;
        case "login":
            login();
        break;
        // 添加商品的判断  皮肤
        case "derma":
            add('skin');
        break;
        case "zShop":
            add('zhuye');
        break;
        // 添加英雄的判断 
        case "zhuyeHero":
            add('hero');
        break;
        // 添加周边
        case 'addRim':
            add('rim');
        break;
        // 添加热门
        case "addHot":
            add('hot');
        break;
        // 返回用户所有的数据
        case "uesr":
            shop('login');
        break;
        // 改变用户的登陆状态
        case "sta":
            sta();
        break;
        //修改密码
        case 'change':
            change();
        break;
        // 添加购物车的判断
        case "addcar":
            addCar();
        break;
        // 获取用户购物车中的东西
        case "gain":
            gain();
        break;
        // 购物车获取数据 只获取一行
        case "obtainskin":
            obtain('skin');
        break;
        // 主页
        case "obtainzhuye":
            obtain('zhuye');
        break;
        // 英雄
        case "obtainhero":
            obtain('hero');
        break;
        // 周边
        case "obtainrim":
            obtain('rim');
        break;
        // 热门
        case "obtainhot":
            obtain('hot');
        break;
        // 修改用户购物车
        case "revision":
            revision();
        break;
        // 删除用户购物车数据
        case "delshop":
            delshop();
        break;
    }
    // 关闭数据库连接
    $con->close();

    // 取到所有的数据
    function shop($h){
        // 全局变量
        global $con;
        // 进行全局查找
        $spl="SELECT * FROM $h WHERE 1";
        // 执行查找
        $res=$con->query($spl);
        // 用来承接的数组
        $arr=[];
        // 输出每行数据 
        while($row = $res->fetch_assoc()) { 
            $count=count($row);//不能在循环语句中，由于每次删除row数组长度都减小 
            for($i=0;$i<$count;$i++){ 
                unset($row[$i]);//删除冗余数据 
            } 
            array_push($arr,$row); 
    
        } ;
        echo json_encode($arr,JSON_UNESCAPED_UNICODE);
    }

    // 删除页面元素的函数
    function del($a){
        // 全局变量
        global $con;
        // 获取id值
        $id=$_POST["id"];
        $spl="DELETE FROM `$a` WHERE `id`='{$id}'";
        // 执行
        $res=$con->query($spl);
        echo $id;
        if($res){
            echo "删除成功";
        }else{
            echo "删除失败";
        }
        
    }
    // 修改的函数
    function alter($e){
        // 获取全局变量
        global $con;
        // 获取传输的数据
        // 获取id
        $id=$_POST["id"];
        //name
        $name=$_POST["name"];
        // url
        $url=$_POST["url"];
        // price
        $price=$_POST["price"];
        // wxprice
        $wxprice=$_POST["wxprice"];
        // echo $name;
        // 更新数据库
        $spl=" UPDATE $e SET `name`='{$name}',`url`='{$url}',`price`='{$price}',`wxprice`='{$wxprice}' WHERE `id`='{$id}'";
        // 执行语句
        $res=$con->query($spl);
        if($res){
            echo "{code:2,'修改成功'}";
        }
    }

    // 注册的函数
    function sign(){
        // 全局变量
        global $con;
        // 获取传输过来的值
        // 账号
        $name=$_POST["name"];
        // 密码
        $pass=$_POST["pass"];
        // 电话
        $phone=$_POST["phone"];
        // 获取表中的数据  进行比较
        $sql = "SELECT `id`, `username`, `password`, `phone`, `shop` FROM `login` WHERE `phone`='{$phone}'";
        $res=$con->query($sql);
        $q=mysqli_num_rows($res);
        if($q>0){
             echo "用户名重复";
        }else{
            $spll= "INSERT INTO `login`(`username`, `password`, `phone`) VALUES ('{$name}','{$pass}','{$phone}')";
            $res1=$con->query($spll);
            if($res1){
                echo "注册成功";
            }else{
                echo "注册失败";
            }
        }
    }

    // 登陆的判断的函数
    function login(){
        // 全局变量
        global $con;
        // 获取数据
        $name=$_POST['name'];
        $pass=$_POST['pass'];
        // 进行判断
        $spl="SELECT * FROM `login` WHERE username='{$name}' and password='{$pass}' and `sta`='true'";
        $res=$con->query($spl);
        $q=mysqli_num_rows($res);
        if($q){
            echo "{code:0;message:'登陆成功'}";
        }else{
            echo "登陆失败,用户账号已被冻结";
        }
    }
    // 添加商品的函数
    function add($a){
        // 全局变量
        global $con;
        // 获取传输过来的值
        //name
        $name=$_POST["name"];
        // url
        $url=$_POST["url"];
        // price
        $price=$_POST["price"];
        // wxprice
        $wxprice=$_POST["wxprice"];
        // 给数据库添加
        $spl= "INSERT INTO `$a`(`name`, `url`, `price`,`wxprice`) VALUES ('{$name}','{$url}','{$price}','{$wxprice}')";
        // 执行语句
        $res=$con->query($spl);
        if($res){
            // 添加成功
            echo "{code:1,添加成功}";
        }else{
            // 添加失败
            echo $res;
        }
    }
    

    // 改变用户登陆状态的函数
    function sta(){
        // 全局变量
        global $con;
        // 获取数据
        $t=$_POST["id"];
        // 获取id
        $id=$_POST["num"];
        // 判断传过来的数字是什么 改变方式是什么
        switch($t){
            case 1:
                $a='false';
            break;
            case 0:
                $a='true';
            break;
        }
        // 修改语句
        $spl=" UPDATE  `login` SET `sta`='{$a}' WHERE `id`='{$id}'";
        // 执行语句
        $res=$con->query($spl);
        // 判断是否成功
        if($res){
            echo "修改成功";
        }else{
            echo "修改失败";
        }
    }



    // 修改密码的函数
    function change(){
        //全局变量
        global $con;
        // 获取传输过来的值
        $phone=$_POST["phone"];
        // 新的密码
        $pass=$_POST["pass"];
        // 获取id
        // $id=$_POST["id"];
        // 先判断电话号码有没有在数据库中
        $sql = "SELECT `id`, `username`, `password`, `phone`, `shop` FROM `login` WHERE `phone`='{$phone}'";
        $res=$con->query($sql);
        $q=mysqli_num_rows($res);
        if($q==0){
            echo "电话号码不存在";
        }else{
            // 修改密码
            $spl1=" UPDATE  `login` SET `password`='{$pass}' WHERE `phone`='{$phone}'";
            // 执行
            $res1=$con->query($spl1);
            if($res1){
                echo "修改成功";
            }else{
                echo "修改失败";
            }
        }

    }

    // 添加购物车的函数
    function addCar(){
        // 全局变量
        global $con;
        // 先获取是谁登陆的
        $name=$_POST["name"];
        // 获取传输过来的商品名称
        $shopname=$_POST["shop"];
        // 添加的数量
        $num=$_POST["num"];
        // 那种类型的商品
        $film=$_POST["film"];
        $arr=[$film,$shopname,$num];
        $r3=implode(",",$arr);
        // 先获取原有加入购物车的东西
        $sql = "SELECT * FROM `login` WHERE `username`='{$name}'";
        // 执行
        $res=$con->query($sql);
        //转成数组
        $b=$res->fetch_assoc()['shop'];
        $a = explode(",", $b);
        // echo sizeof($a);
        if(sizeof($a)==1){
            $s=$r3;
        }else{
            $bz = array_chunk($a, 3);
            // 循环遍历数组
            for($i=0;$i<sizeof($bz);$i++){
                if($bz[$i][0]==$film&&$bz[$i][1]==$shopname){
                    $bz[$i][2]+=$num;
                break;
                }
                if($i==sizeof($bz)-1){
                    echo 000000;
                    array_push($bz,$arr);
                }
            }
    
            foreach ($bz as $val) {
                $val = join(",",$val);
                $temp[] = $val;
            }
            $s=implode(",",$temp);

        }
        // echo json_encode($temp,JSON_UNESCAPED_UNICODE);

        // echo $s;
        $spl1=" UPDATE  `login` SET `shop`='{$s}' WHERE `username`='{$name}'";
        $res1=$con->query($spl1);
        if($res1){
            echo '添加成功';
        }else{
            echo  '添加失败';
        }
        // }
    }   
    // 获取用户购物车的函数
    function gain(){
        // 全局变量
        global $con;
        // 获取用户名
        $name=$_POST["name"];
        // 获取购物车中的东西
        $spl="SELECT * FROM `login` WHERE `username`='{$name}'";
        //执行
        $res=$con->query($spl);
        $q=mysqli_num_rows($res);

        $q=$res->fetch_assoc()['shop'];
        // echo $q; 
        // var_dump($q);
        // 转成字符串输出
        $a = explode(",", $q);
        $bz = array_chunk($a, 3);
        echo json_encode($bz,JSON_UNESCAPED_UNICODE);
    }

    // 购物车获取内容的函数
    function obtain($q){
        // 全局变量
        global $con;
        // 获取商品id
        $id=$_POST["id"];
        // 从数据库中获取元素
        $spl="SELECT * FROM $q WHERE `id`='{$id}'";
        // 执行
        $res=$con->query($spl);
        $p=$res->fetch_assoc();
        echo json_encode($p,JSON_UNESCAPED_UNICODE);

    }

    // 修改用户购物的函数
    function revision(){
        // 全局变量
        global $con;
        // 获取用户名
        $name=$_POST['name'];
        // 获取商品类型 和id
        $shopname=$_POST["shopname"];
        $id=$_POST["id"];
        // 商品总量
        $num=$_POST["num"];
        // 先获取原有加入购物车的东西
        $sql = "SELECT * FROM `login` WHERE `username`='{$name}'";
        // 执行
        $res=$con->query($sql);
        // 获取整行数据
        $q=$res->fetch_assoc()['shop'];
        // var_dump($q);
        // 转成数组
        $a = explode(",", $q);
        // 三个一组转成二维数组
        $z = array_chunk($a, 3);
        // var_dump($z);
        // 便利数组
        for($a =0;$a<sizeof($z);$a++){
            // var_dump($z[$a][0]);
            // 判断
            if($z[$a][0]==$shopname&&$z[$a][1]==$id){
                $z[$a][2]=$num;
            }
        }
        
        // var_dump($z);
        // 转成字符串

        foreach ($z as $val) {
            $val = join(",",$val);
            $temp[] = $val;
        }
        $s=implode(",",$temp);
        echo $s;
        // 删除数据库的内容
        $spl1=" UPDATE  `login` SET `shop`='{$s}' WHERE `username`='{$name}'";
        $res1=$con->query($spl1);
        if($res1){
            echo '修改成功';
        }else{
            echo  '修改失败';
        }

    }
    // 删除用户购物车的函数
    function delshop(){
        // 全局变量
        global $con;
        //获取用户名 商品类型  商品id
        $name=$_POST["name"];
        $id=$_POST["id"];
        $deltype=$_POST["deltype"];
        // 获取这个用户存储的数据
        $sql = "SELECT * FROM `login` WHERE `username`='{$name}'";
        // 执行
        $res=$con->query($sql);
        // 获取整行数据
        $q=$res->fetch_assoc()['shop'];
        // var_dump($q);
        // 转成数组
        $a = explode(",", $q);
        // 三个一组转成二维数组
        $z = array_chunk($a, 3);
        // var_dump($z);
        // 循环便利数组  删除这个元素
        for($a =0;$a<sizeof($z);$a++){
            // var_dump($z[$a][0]);
            // 判断
            if($z[$a][0]==$deltype&&$z[$a][1]==$id){
                // 获取下标
                $key = array_search($z[$a], $z);
                // 删除对应下标的数据
                unset($z[$key]);
                // 重新建立索引
                $z = array_values($z);
            }
        }
        // echo $key;
        var_dump($z);
        // 将删除之后的数据存进数据库中
        foreach ($z as $val) {
            $val = join(",",$val);
            $temp[] = $val;
        }
        $s=implode(",",$temp);
        // echo $s;
        // 删除数据库的内容
        $spl1=" UPDATE  `login` SET `shop`='{$s}' WHERE `username`='{$name}'";
        $res1=$con->query($spl1);
        if($res1){
            echo '修改成功';
        }else{
            echo  '修改失败';
        }
    }
?>