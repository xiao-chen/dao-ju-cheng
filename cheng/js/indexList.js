// 搜索框js
console.log(1)
var searchFlag = true;
$(".searchInp").click(function(){
    searchFlag=false
})
$(document).click(function(){
    if(searchFlag){
        $(".searchInp").val("输入道具进行搜索")
    }else{
        $(".searchInp").val("")
        searchFlag=true;
    }
})

// 主页商品列表
var a;
// 发送ajax获取首页商品列表
$.ajax({
    type: "post",
    url: "../php/2php.php",
    async: true,
    data: {
        type: "shop",
    },
    success: function (res) {
        a = JSON.parse(res);
        // 创建限时抢购商品列表
        createFlashSale(a);
        // 创建默认的新品上架、热门推荐商品列表
        createNewShop(a);
        // 创建主页底部周边商城商品列表
        createAround(a);
    }
})

// 发送ajax获取热门排行商品列表
$.ajax({
    type: "post",
    url: "../php/2php.php",
    async: true,
    data: {
        type: "hotShop",
    },
    success: function (res) {
        var b = JSON.parse(res);
        // 调用创建热门排行的函数
        createHotRank(b);
    }
})

// 新品商品部分按键事件
$(".newShopBtn").click(function () {
    $(".newShopList").html("");
    createNewShop(a);
    $(this).next().css({
        "border": "none",
        "font-weight": "normal",
        "color": "#b8b8b8"
    });
    $(this).css({
        "border-bottom": "2px solid #36ab87",
        "font-weight": "bold",
        "color": "#555555"
    });
})
// 热门商品部分按键事件
$(".hotTitleBtn").click(function () {
    $(".newShopList").html("");
    createHotShop(a);
    $(this).prev().css({
        "border": "none",
        "font-weight": "normal",
        "color": "#b8b8b8"
    });
    $(this).css({
        "border-bottom": "2px solid #36ab87",
        "font-weight": "bold",
        "color": "#555555"
    });
})

// 创建限时抢购的商品列表
function createFlashSale(x) {
    for (var i = 0; i < 4; i++) {
        var li = $('<li><img src = "../img/indeximg/' + x[i].url + '"><p class="heroName">' + x[i].name + '</p><p class="heroPrice"><b>' + x[i].price + '</b>元<span>(99QB)</span></p><p class="countDown">剩余时间：<span>最后三天<span></p><button class="btnBuy">立即购买</button><div class="priceCountDown"><p>直降</p><p>45.50Q币</p></div></li > ');
        // 放入页面
        $(".flashSaleList").append(li);
    }
}

// 创建新品上架的商品列表
function createNewShop(y) {
    for (var j = 4; j < 12; j++) {
        var li = $('<li><img src="../img/indeximg/' + y[j].url + '" alt=""><p>' + y[j].name + '</p><p class="newShopPrice">Q币价:<span>&nbsp&nbsp&nbsp&nbsp' + y[j].price + '&nbspQ币</span><p><p class="newShopPrice">微信价:<span>&nbsp&nbsp&nbsp￥</span><span>' + y[j].wxprice + '</span></p><button class="btnBuyHot">立即购买</button>');
        // 放入页面
        $(".newShopList").append(li);
    }
}

// 创建热门商品的商品列表
function createHotShop(y) {
    for (var j = 11; j > 3; j--) {
        var li = $('<li><img src="../img/indeximg/' + y[j].url + '" alt=""><p>' + y[j].name + '</p><p class="newShopPrice">Q币价:<span>&nbsp&nbsp&nbsp&nbsp' + y[j].price + '&nbspQ币</span><p><p class="newShopPrice">微信价:<span>&nbsp&nbsp&nbsp￥</span><span>' + y[j].wxprice + '</span></p><button class="btnBuyHot">立即购买</button>');
        // 放入页面
        $(".newShopList").append(li);
    }
}

// 创建右侧热门排行列表
function createHotRank(z) {
    for (var i = 0; i < 8; i++) {
        // 判断放入页面中
        if (i < 4) {
            // 创建元素
            var box = $('<div class="hotListBox"><img src="../img/hotimg/' + z[i].url + '" alt=""><p>' + z[i].name + '</p><p>Q币价:&nbsp&nbsp<span>' + z[i].price + '</span>&nbspQ币</p><p>微信价:&nbsp&nbsp￥<span>' + z[i].wxprice + '</span></p><div class="serialNum">0' + (i + 1) + '</div>')
            // 放入页面
            $(".hotRank .hotList").append(box);
        } else {
            // 创建元素
            var box = $('<div class="hotListBox"><img src="../img/hotimg/' + z[i].url + '" alt=""><p>' + z[i].name + '</p><p>Q币价:&nbsp&nbsp<span>' + z[i].price + '</span>&nbspQ币</p><p>微信价:&nbsp&nbsp￥<span>' + z[i].wxprice + '</span></p>')
            // 放入页面
            $(".hotLike .hotList").append(box);
        }
    }
}

// 创建周边商城内容
function createAround(w){
    for(var i=12;i<16;i++){
        var li=$('<li><img src="../img/indeximg/'+w[i].url+'" alt=""><p>'+w[i].name+'<span><i>￥</i>'+w[i].price+'</span></p></li>');
        // 放入页面
        $(".aroundList").append(li);
    }
}

// 主页底部精彩活动部分点赞事件
$(".clickLike").click(function(){
    // 防止多次点击事件
    if($(this).css("backgroundPosition")!=="-20px -168px"){
        $(this).css("backgroundPosition","-20px -168px");
        // 获取当前点赞数
        var num=$(this).next().text();
        // 点赞数加一
        num-=(-1);
        $(this).next().text(num);
    }
})
// 判断电脑屏幕分辨率来调整body的背景图
if(parseInt($("body").css("width"))>1700){
    $("body").css("background-size","100% 28.3%")
}